# elasticsearch
POC Elasticsearch / symfony 4

Ceci est un POC sur Elasticsearch, dans le but d'appréhender son utilisation et 
son implémentation. Parce qu'il ne faut pas s'approprier le travail d'autrui, 
j'ai suivi le tutoriel suivant : https://afsy.fr/avent/2017/20-elasticsearch-6-et-symfony-4
(J'ai apporté une correction dans ce dépôt par rapport au tutoriel, en effet dans
la définition du mapping entité - document elasticsearch, il faut définir les entités comme 'article' et non 'articles')

Il y aurait bien des choses à améliorer, mais il y a aussi bien des choses auxquels j'aimerai me former....

La suite de ce document est en Anglais, j'y ai ajouté quelques tips à la fin du Readme.

Symfony Demo Application
========================

The "Symfony Demo Application" is a reference application created to show how
to develop applications following the [Symfony Best Practices][1].

Requirements
------------

  * PHP 7.1.3 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

Install the [Symfony client][4] binary and run this command:

```bash
$ symfony new --demo my_project
```

Alternatively, you can use Composer:

```bash
$ composer create-project symfony/symfony-demo my_project
```

Usage
-----

There's no need to configure anything to run the application. If you have
installed the [Symfony client][4] binary, run this command to run the built-in
web server and access the application in your browser at <http://localhost:8000>:

```bash
$ cd my_project/
$ symfony serve
```

If you don't have the Symfony client installed, run `php bin/console server:run`.
Alternatively, you can [configure a web server][3] like Nginx or Apache to run
the application.

Tests
-----

Execute this command to run tests:

```bash
$ cd my_project/
$ ./bin/phpunit
```

[1]: https://symfony.com/doc/current/best_practices/index.html
[2]: https://symfony.com/doc/current/reference/requirements.html
[3]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html
[4]: https://symfony.com/download



---------------------------------------------------------------------------

On Linux if you have an error in elasticsearch container because too low memory is usable, or if elasticsearch container die, tou can use this command to allow more memory usable by Docker :
```bash
sudo sysctl -w vm.max_map_count=262144
```

In IndexBuilder, we set our index key to '**blog**'

elasticsearch container can be access at http://localhost:9200/
the elasticsearch blog document definition can be access at http://localhost:9200/blog/

To be sure to get our data in kibana (http://localhost:5601), 
use "Dev tools" with this query : 
```
GET blog/_search
{
  "query": {
    "match_all": {}
  }
}
```

The query behind, is more complex, we get all document with **Aliquam erat volutpat** string, in
title, summary, content, ans author fields, but if this string is in title it's better(request string in title : first result)
```
GET blog/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "multi_match": {
            "query": "Aliquam erat volutpat",
            "fields": [
              "title^4",
              "summary",
              "content",
              "author"
            ]
          }
        }
      ]
    }
  }
}
```
